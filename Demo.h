#include <iostream>


using namespace std;

class Investment 
 {
    private:  //properties
         double value;
         float interest_rate; 
         
         
         
	public:
        
			Investment(long double, float);//overloaded constructors
			Investment();//default constructor 
			void bank();
			void setInter(float);
			float getInter();
			void setVal(long double);
			long double getVal();
 };

